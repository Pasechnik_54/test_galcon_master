﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.src.Foundation;

namespace Assets.src.Pool
{
    [AddComponentMenu("Pool/PlanetsPool")]

    public class PlanetsPool : Singleton<PlanetsPool>
    {        
        [SerializeField] private PoolObject TargetPrefab = null;
        
        private ObjectPoolContainer _planetsContainer;        


        public void CreatePlanets(int QuantityPlanets)
        {
            _planetsContainer = new ObjectPoolContainer(QuantityPlanets, TargetPrefab,
                                                       this.transform);
        }

        public GameObject GetPlanet()
        {
            PoolObject refPlanet = _planetsContainer.GetPoolObject();

            GameObject planetObject = refPlanet.gameObject;
            if (planetObject != null)
            {
                //planetObject.SetActive(true);
                return planetObject;                
            }
            else
            {
                throw new Exception("error get planet from pool");
            }
            
        }
    }
}
