﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.src.Foundation;
using Assets.src.Planet.Ships;

namespace Assets.src.Pool
{
    [AddComponentMenu("Pool/AttackShipsPool")]

    public class AttackShipsPool : Singleton<AttackShipsPool>
    {
        [SerializeField] private PoolObject TargetPrefab = null;
        [SerializeField] private int StartedSizePool = 0;

        private ObjectPoolContainer _shipsContainer;

        private void Start()
        {
            _shipsContainer = new ObjectPoolContainer(StartedSizePool, TargetPrefab,
                                                       this.transform);
        }

        public AttackShip GetAttackShip()
        {
            PoolObject shipObj = _shipsContainer.GetPoolObject();

            AttackShip ship = shipObj.GetComponent<AttackShip>();

            if (ship != null)
            {
                return ship;
            }
            else
            {
                throw new Exception("Fault pool " + this.name);
            }            
        }

    }
}
