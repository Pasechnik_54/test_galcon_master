﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.src.Foundation;


namespace Assets.src.Pool
{
    [AddComponentMenu("Pool/TimersPool")]

    public class TimersPool : Singleton<TimersPool>
    {
        [SerializeField] private PoolObject TargetPrefab = null;
        [SerializeField] private int StartedSizePool = 0;               

        private ObjectPoolContainer _cycleTimersPool;


        private void Start()
        {
            _cycleTimersPool = new ObjectPoolContainer( StartedSizePool, TargetPrefab,
                                                        this.transform );
        }
            
        public CycleTimer GetCycleTimer()
        {
            PoolObject refTimer = _cycleTimersPool.GetPoolObject();

            CycleTimer timer = refTimer.GetComponent<CycleTimer>();            

            if ( timer != null)
            {
                //timer.gameObject.SetActive(true);
                return timer;
            }
            else
            {           
                throw new Exception("Fault pool for cycle timers ");
            }
            
        }

    }
}
