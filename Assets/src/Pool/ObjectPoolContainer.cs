﻿using System;
using System.Collections.Generic;
using System.Collections;

using UnityEngine;

namespace Assets.src.Pool
{

	public class ObjectPoolContainer : IGetterFromPool
	{

		private List<PoolObject> _objectsList; // список объектов
		private Transform _transformParent; // родительский трансформ


		public ObjectPoolContainer( int startedSizePool, PoolObject sample, 
									Transform targetParentTransform )
		{
			_transformParent = targetParentTransform;
			_objectsList = new List<PoolObject>();

			for (int i = 0; i < startedSizePool; i++)
			{
				AddObject(sample, _transformParent); //создаем объекты до указанного количества
			}
		}


		public PoolObject GetPoolObject()
		{
			for (int i = 0; i < _objectsList.Count; i++)
			{
				if (_objectsList[i].gameObject.activeInHierarchy == false)
				{
					_objectsList[i].gameObject.SetActive(true);
					return _objectsList[i];
				}
			}

			AddObject(_objectsList[0], _transformParent);

			return _objectsList[_objectsList.Count - 1];
		}

		private void AddObject(PoolObject sample, Transform transformParent)
		{
			GameObject currObj = GameObject.Instantiate(sample.gameObject);

			currObj.name = sample.name;
			currObj.transform.SetParent(transformParent);

			_objectsList.Add(currObj.GetComponent<PoolObject>());
			currObj.SetActive(false);
		}
	}
}
