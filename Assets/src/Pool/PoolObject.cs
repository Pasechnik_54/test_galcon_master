﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.src.Pool
{
	[AddComponentMenu("Pool/PoolObject")]

	public class PoolObject : MonoBehaviour
	{
		public void ReturnToPool()
		{
			gameObject.SetActive(false);
		}
	}
}
