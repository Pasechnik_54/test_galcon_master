﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.src.Foundation;
using Assets.src.Pool;
using Assets.src.Planet;

using NetObject = System.Object;

namespace Assets.src.Controllers
{
    [AddComponentMenu("Controllers/GamePlayController")]

    public class GamePlayController : MonoBehaviour
    {
        // между этими значения ми будет кидаться рандом 
        // для стартового кол-ва кораблей на каждой планете (кроме стартовой планеты игрока)
        [SerializeField] private int MinStartedCountShipsInNeutralPlanet = 0;
        [SerializeField] private int MaxStartedCountShipsInNeutralPlanet = 128;
        
        // для планеты игрока задаем начальное кол-во кораблей здесь
        [SerializeField] private int StartedCountShipsForPlayerPlanet = 50;


        private List<PlanetComponent> _checkedPlayerPlanets = null;
        private List<PlanetComponent> _activePlanets = null;
                

        private PlanetController _planetController;
        public void SetPlanetController(PlanetController planetControl)
        {
            _planetController = planetControl;
        }

        

        private void Awake()
        {
            _activePlanets = new List<PlanetComponent>();
            _checkedPlayerPlanets = new List<PlanetComponent>();
        }

        public void StartInitGameScene()
        {
            GetActivePlanetsList();
            StartedInitPlanets();
        }


        private void GetActivePlanetsList()
        {
            List<GameObject> planetsObjList = _planetController.ActivePlanetsList;

            // получаем список активных планет и делаем стартовую инициализацию
            foreach (var planet in planetsObjList)
            {
                var planetComp = planet.GetComponent<PlanetComponent>();
                if (planetComp == null)
                {
                    throw new Exception("planet component not found");
                }

                _activePlanets.Add(planetComp);                
            }
        }

        private int CalcIndexStartedPlayerPlanet()
        {
            int minIndex = 0;
            int maxIndex = _activePlanets.Count - 1;

            int index = UnityEngine.Random.Range(minIndex, maxIndex);

            return index;
        }

        private void StartedInitPlanets()
        {

            foreach (var planet in _activePlanets)
            {
                // подписываемся на клики по планетам
                planet.PlanetClicked += new EventHandler(PlanetOnClicked);

                int startCount = UnityEngine.Random.Range( MinStartedCountShipsInNeutralPlanet, 
                                                           MaxStartedCountShipsInNeutralPlanet );
                
                // распределяем корабли, сначала все нейтральные
                planet.SetStartedPlanetState(startCount, PlanetBind.Neutral);
                
            }

            // стартовая планета игрока
            StartPlayerPlanetInit();
        }

        private void StartPlayerPlanetInit()
        {
            int indexStartPlayerPlanet = CalcIndexStartedPlayerPlanet();
            var startPlayerPlanet = _activePlanets[indexStartPlayerPlanet];
            startPlayerPlanet.SetStartedPlanetState(StartedCountShipsForPlayerPlanet, PlanetBind.Player);
        }



        private void PlanetOnClicked(NetObject sender, EventArgs args)
        {            
            PlanetComponent planet = ((PlanetArgs)args).PlanetComponent;
            
            if (planet.CurrentPlanetBind == PlanetBind.Player)
            {
                if (_checkedPlayerPlanets.Contains(planet) )
                {
                    Debug.Log("planet already checked");
                }
                else
                {
                    _checkedPlayerPlanets.Add(planet);
                }
                
            }
            else if ( planet.CurrentPlanetBind == PlanetBind.Enemy ||
                      planet.CurrentPlanetBind == PlanetBind.Neutral )
            {
                if ( _checkedPlayerPlanets.Count != 0)
                { 
                    AttackTargetPlanet(planet);
                }                                
            }
                        
        }

        private void AttackTargetPlanet(PlanetComponent planet)
        {
            Debug.Log("Command attack target planet");            

            //Vector2 targetPos = planet.transform.position;

            foreach (var playerAttackUnit in _checkedPlayerPlanets)
            {
                playerAttackUnit.SendShipsToAttack(planet);
            }

            ClearAllCheckingPlanets();
        }

        private void ClearAllCheckingPlanets()
        {
            _checkedPlayerPlanets.Clear();

            foreach (var planet in _activePlanets)
            {
                planet.ClearChekPlanet();
            }
        }

        private void OnDestroy()
        {
            
            foreach (var planet in _activePlanets)
            {
                if ( planet != null)
                {
                    // отписываемся от кликов
                    planet.PlanetClicked -= PlanetOnClicked;
                }
            }
        }
    }
}
