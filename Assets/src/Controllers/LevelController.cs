﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Assets.src.Planet;
using Assets.src.Pool;
using Assets.src.Foundation;


namespace Assets.src.Controllers
{
    [RequireComponent( typeof(GamePlayController)) ]
    [AddComponentMenu("Controllers/LevelController")]

    public class LevelController : MonoBehaviour
    {
        [SerializeField] GameObject PlanectControllerObject;

        private PlanetController _planetController;

        private GamePlayController _gameController;

        void Start()
        {
            _planetController = PlanectControllerObject.GetComponent<PlanetController>();
            if (_planetController == null)
            {
                throw new Exception("planet generator not found");
            }
            _gameController = GetComponent<GamePlayController>();
            if (_gameController == null)
            {
                throw new Exception("gameplay controller not found");
            }
            else
            {
                _gameController.SetPlanetController(_planetController);
            }
        }        

        public void ReloadLevel()
        {            
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);            
        }

        public void StartLevel()
        {
            _planetController.GeneratePlanets();
            _gameController.StartInitGameScene();
        }

        public void QuitLevel()
        {
            Application.Quit();
        }
        
    }
}
