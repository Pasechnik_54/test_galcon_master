﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonsController : MonoBehaviour
{
    [SerializeField] private GameObject PlayButton;
    [SerializeField] private GameObject ReplayButton;

    public void PlayButtonOnClicked()
    {
        Debug.Log("Play button clicked");
        PlayButton.SetActive(false);
        ReplayButton.SetActive(true);
    }

    public void ReplayButtonOnClicked()
    {
        Debug.Log("Replay button clicked");
    }

    public void ExitButtonOnClicked()
    {
        Debug.Log("Exit button clicked");
    }
}
