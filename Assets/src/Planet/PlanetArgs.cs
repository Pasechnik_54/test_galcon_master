﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Assets.src.Planet
{
    class PlanetArgs : EventArgs
    {
        public PlanetComponent PlanetComponent { get; private set; }

        public PlanetArgs(PlanetComponent senderPlanetComponent)
        {
            PlanetComponent = senderPlanetComponent;
        }

    }
}
