﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

using Assets.src.Pool;
using Assets.src.Planet.Ships;

using NetObj = System.Object;


namespace Assets.src.Planet
{
    [RequireComponent( typeof(ShipsFactoryComponent) )] // для создания кораблей
    [RequireComponent(typeof(ShipsAttackComponent) )] // для отправки кораблей
    [RequireComponent( typeof(BindColorModificator) )] // для обозначения привязки планеты
    [RequireComponent( typeof(HighlightComponent) )] // для выделения планеты
    [AddComponentMenu("Planet/PlanetComponent")]

    public class PlanetComponent : PoolObject
    {               
        // компонент для отображения текущего значения счетчика кораблей
        [SerializeField] private GameObject CountShipsDisplay;        
        // принадлежность планеты
        public PlanetBind CurrentPlanetBind { get { return _currBind; } }
        public void SetPlanetBind(PlanetBind targetBind)
        {
            _currBind = targetBind;
            SetColorOfBindState();

            switch (_currBind)
            {
                case PlanetBind.Neutral:
                    Debug.Log(" set planet to Neutral");
                    break;
                case PlanetBind.Player:
                    Debug.Log(" set planet to Player");
                    if (_shipSource != null) StartCreateShips();
                    break;
                case PlanetBind.Enemy:
                    Debug.Log(" set planet to Enemy");
                    throw new NotImplementedException("required enemy inplementation");
                    //break;
            }

        }
        private PlanetBind _currBind = PlanetBind.Neutral;


        public EventHandler PlanetClicked;

                
        private TextMeshProUGUI _countShipsTMP;
        private ShipsFactoryComponent _shipSource;
        private BindColorModificator _colorMod;
        private HighlightComponent _highlight;
        private ShipsAttackComponent _shipsAttackComponent;

        private int _startedShipsCount = 0;


        private void Start()
        {
            InitDisplayShipsCountComponent();
            InitShipsComponent();
            InitColorModComponent();
            InitHighlightComponent();
            InitShipsAttackComponent();
        }

        private void InitDisplayShipsCountComponent()
        {
            _countShipsTMP = CountShipsDisplay.GetComponent<TextMeshProUGUI>();

            if (_countShipsTMP != null)
            {
                const int DEFAULT_COUNT = 0;
                _countShipsTMP.SetText(DEFAULT_COUNT.ToString());
            }
            else
            {
                throw new Exception("Error TextMeshPro references");
            }
        }

        private void InitShipsComponent()
        {
            _shipSource = GetComponent<ShipsFactoryComponent>();
            if (_shipSource == null)
            {
                throw new Exception("ship source not found");
            }
            else
            {
                _shipSource.ChangeQuantityShips += new EventHandler(OnChangeQuantityShips);
                _shipSource.SetShipsCount(_startedShipsCount);

                if (_currBind == PlanetBind.Player)
                {
                    StartCreateShips();
                }
            }
        }

        private void InitColorModComponent()
        {
            _colorMod = GetComponent<BindColorModificator>();
            if (_colorMod == null)
            {
                throw new Exception("color modificator not found");
            }
            else
            {
                if (_currBind == PlanetBind.Player)
                {
                    SetColorOfBindState();
                }
            }
        }

        private void InitHighlightComponent()
        {
            _highlight = GetComponent<HighlightComponent>();
            if (_colorMod == null)
            {
                throw new Exception("highlight component not found");
            }            
        }

        private void InitShipsAttackComponent()
        {
            _shipsAttackComponent = GetComponent<ShipsAttackComponent>();
            if (_shipsAttackComponent == null)
            {

                throw new Exception("ships attack component not found");
            }
        }

        private void OnMouseUp()
        {
            if (_currBind == PlanetBind.Player)
            {
                SetCheckPlanet(true);
            }

            PlanetClicked?.Invoke( this, new PlanetArgs(this) );
        }


        public void SetStartedPlanetState(int startShipsCount, PlanetBind targetBind)
        {            
            _startedShipsCount = startShipsCount;
            _currBind = targetBind;

            if (_shipSource != null)
            {                
                _shipSource.SetShipsCount(_startedShipsCount);
            }
        } 
        

        public void ClearChekPlanet()
        {
            SetCheckPlanet(false);
        }
        

        public void SendShipsToAttack(PlanetComponent targetPlanet)
        {
            Debug.Log(string.Format("ships attak position: x={0} y={1}",
                                    targetPlanet.transform.position.x,
                                    targetPlanet.transform.position.y) );

            _shipsAttackComponent.Attack(targetPlanet);
        }

        public void DamagePlanet(int inputDamage)
        {
            Debug.Log(string.Format(" inputed damage = {0}", inputDamage) );

            int deltaShips = _shipSource.QuantityShips - inputDamage;

            if (deltaShips > 0)
            {
                _shipSource.SetShipsCount(deltaShips);
            }
            else
            {
                _shipSource.SetShipsCount(-deltaShips);
                SetPlanetBind(PlanetBind.Player);
            }

        }

        public void PumpPlanet(int inputPumpDamage)
        {
            Debug.Log(string.Format(" inputed pumpValue = {0}", inputPumpDamage));

            _shipSource.SetShipsCount(_shipSource.QuantityShips + inputPumpDamage);
        }



        private void StartCreateShips()
        {
            Debug.Log(this.name + " start create ships");
            _shipSource.RunCreateShips();
        }

        private void OnChangeQuantityShips(NetObj sender, EventArgs args)
        {            
            _countShipsTMP.SetText( _shipSource.QuantityShips.ToString() );
        }

        private void SetColorOfBindState()
        {
            switch (_currBind)
            {
                case PlanetBind.Neutral:
                    _colorMod.SetNeutralColor();
                    break;
                case PlanetBind.Player:
                    _colorMod.SetPlayerColor();
                    break;
                case PlanetBind.Enemy:
                    _colorMod.SetEnemyColor();
                    break;
            }
        }

        private void SetCheckPlanet(bool state)
        {
            if (state == true)
            {
                _highlight.HighlightOn();
            }
            else
            {
                _highlight.HighlightOff();
            }

        }


        private void OnDestroy()
        {
            if (_shipSource != null)
            {
                _shipSource.ChangeQuantityShips -= OnChangeQuantityShips;
            }
        }

    }
}
