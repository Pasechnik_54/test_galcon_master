﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

namespace Assets.src.Planet.Positioner
{
    struct RangePosition
    {
        public Vector2 minPos;
        public Vector2 maxPos;
    }
}
