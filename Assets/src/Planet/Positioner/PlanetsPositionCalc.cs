﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

namespace Assets.src.Planet.Positioner
{
    class PlanetsPositionCalc
    {
        private List<Vector2> _leftSidePositionTable;
        private List<Vector2> _rightSidePositionTable;

        RegionsPositionCalc _posCalc;

        public PlanetsPositionCalc( int leftSidePlanetCount, int rightSidePlanetCount,
                                  float bordermargin )
        {
            _leftSidePositionTable = new List<Vector2>(leftSidePlanetCount);
            _rightSidePositionTable = new List<Vector2>(rightSidePlanetCount);

            _posCalc = new RegionsPositionCalc(bordermargin);
        }


    }
}
