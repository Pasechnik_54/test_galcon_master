﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

namespace Assets.src.Planet.Positioner
{
    class RegionsPositionCalc
    {
        public RangePosition LeftRangePos { get { return _leftRangePos; } }
        private RangePosition _leftRangePos;

        public RangePosition RightRangePos { get { return _rightRangePos; } }
        private RangePosition _rightRangePos;


        public RegionsPositionCalc(float maxPlanetRadius)
        {            
            CalcPositionAreas(maxPlanetRadius);
        }        


        private void CalcPositionAreas(float borderMargin)
        {
            Vector2 bottomLeft = Camera.main.ViewportToWorldPoint(new Vector2(0, 0)); 
            Vector2 topRight = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
            Vector2 bottomCenter = Camera.main.ViewportToWorldPoint(new Vector2(0.5f, 0));
            Vector2 topCenter = Camera.main.ViewportToWorldPoint(new Vector2(0.5f, 1));


            // вычисляем границы областей с учетом отступов от краев
            float centerMargin = borderMargin * 0.7f;             

            _leftRangePos.minPos = new Vector2( (bottomLeft.x + borderMargin), 
                                                    (bottomLeft.y + borderMargin) );//bottomLeft;
            _leftRangePos.maxPos = new Vector2( (topCenter.x - centerMargin),
                                                    (topCenter.y - borderMargin) );//topCenter;

            String leftXRange = String.Format( "left x range: {0} to {1}", 
                                               LeftRangePos.minPos.x.ToString(),
                                               LeftRangePos.maxPos.x.ToString() );
            String leftYRange = String.Format( "left y range: {0} to {1}",
                                               LeftRangePos.minPos.y.ToString(),
                                               LeftRangePos.maxPos.y.ToString() );
            Debug.Log(leftXRange);
            Debug.Log(leftYRange);


            _rightRangePos.minPos = new Vector2( (bottomCenter.x + centerMargin),
                                                     (bottomCenter.y + borderMargin) );//bottomCenter;
            _rightRangePos.maxPos = new Vector2( (topRight.x - borderMargin),
                                                     (topRight.y - borderMargin) );//topRight;

            String rightXRange = String.Format("right x range: {0} to {1}",
                                               RightRangePos.minPos.x.ToString(),
                                               RightRangePos.maxPos.x.ToString() );
            String rightYRange = String.Format("right y range: {0} to {1}",
                                               RightRangePos.minPos.y.ToString(),
                                               RightRangePos.maxPos.y.ToString());

            Debug.Log(rightXRange);
            Debug.Log(rightYRange);

        }

    }
}
