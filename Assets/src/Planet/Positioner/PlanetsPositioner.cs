﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;


namespace Assets.src.Planet.Positioner
{
    
    class PlanetsPositioner
    {
        private List<Vector2> _leftSidePositionTable;
        private List<Vector2> _rightSidePositionTable;

        private int _leftSidePlanetCount = 0;
        private int _rightSidePlanetCount = 0;

        private RegionsPositionCalc _reigonsCalc;

        public PlanetsPositioner(int leftSidePlanetCount, int rightSidePlanetCount)
        {
            _leftSidePlanetCount = leftSidePlanetCount;
            _rightSidePlanetCount = rightSidePlanetCount;

            _leftSidePositionTable = new List<Vector2>();
            _rightSidePositionTable = new List<Vector2>();

            const float BORDER_MATGIN = 3.0f;
            _reigonsCalc = new RegionsPositionCalc(BORDER_MATGIN);
        }


        // здесь необходима реализация алгоритма на взвешенном графе (для размещения планет)
        // пока будет костыль для тестов (упрощенная генерация через Random по X и разнесение по Y)
        public List<Vector2> GetListLeftSidePlanetPositions()
        {
            CalcPositions(_leftSidePositionTable, _leftSidePlanetCount, _reigonsCalc.LeftRangePos);
            return _leftSidePositionTable;
        }
        public List<Vector2> GetListRightSidePlanetPositions()
        {
            CalcPositions(_rightSidePositionTable, _rightSidePlanetCount, _reigonsCalc.RightRangePos);
            return _rightSidePositionTable;
        }


        private void CalcPositions(List<Vector2> targetPos, int listCount, RangePosition range)
        {
            
            float height = range.maxPos.y - range.minPos.y;
            Debug.Log(String.Format("height region = {0}", height.ToString()));

            const int MAX_COUNT_PLANET_TO_VERTICAL = 4;
            float stepToY = height / MAX_COUNT_PLANET_TO_VERTICAL;

            for (int i = 0; i < listCount; i++)
            {
                float offsetToY = stepToY * i; // на первой итерации смещение нулевое
                float y = range.minPos.y + offsetToY;

                float x = UnityEngine.Random.Range(range.minPos.x, range.maxPos.x);

                targetPos.Add( new Vector2(x, y) );
            }
        }


    }
}
