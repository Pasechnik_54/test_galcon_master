﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.src.Planet
{
    [AddComponentMenu("Planet/HighlightComponent")]

    public class HighlightComponent : MonoBehaviour
    {
        [SerializeField] private GameObject HighlightObject;

        public void HighlightOn()
        {
            HighlightObject.SetActive(true);
        }

        public void HighlightOff()
        {
            HighlightObject.SetActive(false);
        }
    }
}
