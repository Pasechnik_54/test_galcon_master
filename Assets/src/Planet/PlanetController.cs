﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.src.Pool;
using Assets.src.Planet.Positioner;

namespace Assets.src.Planet
{
    //[RequireComponent( typeof(PlanetsPool) )] // для хранения созданных планет
    [AddComponentMenu("Planet/PlanetController")]

    public class PlanetController : MonoBehaviour
    {
        [SerializeField] private int MinQuantityPlanets = 6;
        [SerializeField] private int MaxQuantityPlanets = 9;


        private PlanetsPool _planetsPool;
        private PlanetsPositioner _positioner;

        public List<GameObject> ActivePlanetsList { get { return _activePlanets; } }
        private List<GameObject> _activePlanets; // здесь активные планеты

        private int _fullPlanetCount = 0;
        private int _leftPlanetsCount = 0;
        private int _rightPlanetsCount = 0;


        private void Awake()
        {
            _activePlanets = new List<GameObject>();
        }

        void Start()
        {
            _planetsPool = PlanetsPool.Instance; 
        }

        public void GeneratePlanets()
        {
            
            CalcFullPlanetsCount();
            CalcPlanetsToSidesCountes();            

            _positioner = new PlanetsPositioner( _leftPlanetsCount, _rightPlanetsCount);

            List<Vector2> leftListPos = _positioner.GetListLeftSidePlanetPositions();
            List<Vector2> rightListPos = _positioner.GetListRightSidePlanetPositions();

            _planetsPool.CreatePlanets(_fullPlanetCount);

            PlacePlanets(leftListPos);
            PlacePlanets(rightListPos);
        }


        private void CalcFullPlanetsCount()
        {
            float rand = Random.Range(MinQuantityPlanets, MaxQuantityPlanets);

            _fullPlanetCount = (int)rand;
            Debug.Log("planet count = " + _fullPlanetCount.ToString());
        }

        private void CalcPlanetsToSidesCountes()
        {
            float mid = _fullPlanetCount / 2; // пополам
            // здесь кидаем рандом на смещение ( как будут распределены планеты по сторонам
            float randOffset = Random.Range((mid - 1), (mid + 1) );

            // распределяем планеты с учетом смещения
            _leftPlanetsCount = (_fullPlanetCount - (int)randOffset);
            Debug.Log("left side planet = " + _leftPlanetsCount.ToString());
            _rightPlanetsCount = (_fullPlanetCount - _leftPlanetsCount);
            Debug.Log("right side planet = " + _rightPlanetsCount.ToString());
        }

        private void PlacePlanets(List<Vector2> positionsList)
        {
            foreach (var pos in positionsList)
            {
                GameObject planet = _planetsPool.GetPlanet();
                planet.transform.position = pos;
                Debug.Log(string.Format("Planet placed. x = {0}; y = {1}",
                                            pos.x.ToString(), pos.y.ToString()));
                
                _activePlanets.Add(planet); // добавляем планету в список активных
            }
        }


    }
}

