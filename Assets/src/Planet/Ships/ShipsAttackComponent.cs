﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.src.Pool;

namespace Assets.src.Planet.Ships
{
    [RequireComponent(typeof(ShipsFactoryComponent))]
    [AddComponentMenu("Planet/ShipsAttackComponent")]

    public class ShipsAttackComponent : MonoBehaviour
    {
        [SerializeField] int QuantityAttackShipsInPercent = 50;
        [SerializeField] GameObject SpawnPositionsObject;        

        private ShipsFactoryComponent _shipsFactory;        

        private List<Vector3> _listSpawnPositions;
        private AttackShipsPool _attackShipsPool;
        

        private void Awake()
        {
            _listSpawnPositions = new List<Vector3>();
        }

        private void Start()
        {
            _shipsFactory = GetComponent<ShipsFactoryComponent>();
            if ( _shipsFactory == null)
            {
                throw new Exception("ships factory not found");
            }

            _attackShipsPool = AttackShipsPool.Instance;
            if (_attackShipsPool == null)
            {
                throw new Exception("attack ships pool not found");
            }

            var spawnList = SpawnPositionsObject.GetComponentsInChildren<SpawnPlace>();
            if (spawnList == null)
            {
                throw new Exception(" spawn placed not found");
            }
            else
            {
                for (int i = 0; i < spawnList.Length; i++)
                {
                    Vector3 position = spawnList[i].transform.position;
                    _listSpawnPositions.Add(position);
                }
            }
        }

        
        public void Attack(PlanetComponent targetPlanet)
        {

            int attackShipsCount = CalcCountAttackShips();
            Debug.Log(string.Format("requqired {0} ships for attack", attackShipsCount) );
            
            _shipsFactory.RemoveShipsFromAttack(attackShipsCount);
                       
            
            if ( attackShipsCount <= _listSpawnPositions.Count)
            {                
                var attackShipsGroup = GetShipsForAttack(attackShipsCount);

                SendGroupShipsWithMinimalDamageToAttack(attackShipsGroup, targetPlanet);                
            }
            else
            {
                int maxDamageOneShip = CalcMaxDamageOneShip(attackShipsCount);

                var attackShipsGroup = GetShipsForAttack(_listSpawnPositions.Count);
                
                SendGroupShipsWithCalculatedDamage( attackShipsGroup, targetPlanet, 
                                                    maxDamageOneShip, attackShipsCount );
            }
            
        }

        private int CalcCountAttackShips()
        {
            const int MAX_PERCENT_VALUE = 100;
            if (QuantityAttackShipsInPercent > 0 &&
                QuantityAttackShipsInPercent <= MAX_PERCENT_VALUE)
            {
                float fullPlanetShips = _shipsFactory.QuantityShips;
                float count = (fullPlanetShips / MAX_PERCENT_VALUE) * QuantityAttackShipsInPercent;
                return (int)count;
            }
            else
            {
                throw new Exception("Incorrect percent ships value");
            }
            
        }

        private int  CalcMaxDamageOneShip(int attackShipsCount)
        {
            // для более точного округления при делении используем float
            float summDamage = attackShipsCount;
            float maxQuantityShips = _listSpawnPositions.Count;

            float damageOneShip = summDamage / maxQuantityShips;            
            Debug.Log(string.Format("float damage one ship = {0}", damageOneShip.ToString() ) );

            // Convert вместо (int) чтобы не отбрасывать дробную часть, а округлять
            int intDamageOneShip = Convert.ToInt32(damageOneShip); 
            Debug.Log(string.Format("int damage one ship = {0}", intDamageOneShip) );
            return intDamageOneShip;
        }

        private List<AttackShip> GetShipsForAttack(int shipsCount)
        {
            List<AttackShip> shipList = new List<AttackShip>();

            for (int i = 0; i < shipsCount; i++)
            {
                var ship = _attackShipsPool.GetAttackShip();
                shipList.Add(ship);

                Debug.Log(string.Format("ship getted from pool, uid ={0}", ship.GetInstanceID()));
            }

            return shipList;
        }

        private void SendGroupShipsWithMinimalDamageToAttack(List<AttackShip> attackShipsGroup,
                                                             PlanetComponent targetPlanet)
        {   
            for (int i = 0; i < attackShipsGroup.Count; i++)
            {
                var ship = attackShipsGroup[i];
                SetShipInStartedPosition(ship, i);

                const int MINIMAL_DAMAGE = 1;
                ship.SendToAttack(MINIMAL_DAMAGE, targetPlanet);
            }
        }

        private void SendGroupShipsWithCalculatedDamage(List<AttackShip> attackShipsGroup, PlanetComponent targetPlanet, 
                                                        int maxDamage, int targetFullDamage)
        {
            for (int i = 0; i < attackShipsGroup.Count; i++)
            {
                var ship = attackShipsGroup[i];
                SetShipInStartedPosition(ship, i);

                int deltaTargetAndMaxDamage = targetFullDamage - maxDamage;

                if (deltaTargetAndMaxDamage >= 0 )
                {
                    ship.SendToAttack(maxDamage, targetPlanet);
                }
                else
                {
                    // последнему кораблю дамаг может пересчитаться (чтобы сумма дамага сошлась)
                    Debug.Log("!!! recalc damage ");
                    int damage = maxDamage + deltaTargetAndMaxDamage;
                    ship.SendToAttack(damage, targetPlanet);
                }            
            }
        }

        private void SetShipInStartedPosition(AttackShip ship, int indexInSpawnTable)
        {            
            ship.transform.position = _listSpawnPositions[indexInSpawnTable];            
            ship.gameObject.SetActive(true);            
        }
               
    }
}
