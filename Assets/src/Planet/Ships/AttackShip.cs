﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.src.Pool;
using Assets.src.Planet;

namespace Assets.src.Planet.Ships
{
    [AddComponentMenu("Planet/AttackShip")]

    public class AttackShip : PoolObject
    {
        [SerializeField] float SpeedFly = 1.0f;

        public int Damage { get { return _damage;  } }
        private int _damage = 0;

        private PlanetComponent _targetAttackPlanet;

        private Vector3 _targetAttackPosition;
        private Vector3 _moveDirection;

        
        public void SendToAttack(int damage, PlanetComponent targetPlanet)
        {
            _damage = damage;
            _targetAttackPlanet = targetPlanet;
            _targetAttackPosition = _targetAttackPlanet.transform.position;

            _moveDirection = CalcDirection();

            Debug.Log("Ship send to attack");
        }

        

        private void Update()
        {            
            Vector3 translate = _moveDirection * SpeedFly * Time.deltaTime;//new Vector3(SpeedFly * Time.deltaTime, 0, 0 );
            this.transform.Translate(translate);
        }

        private void FixedUpdate()
        {
            _moveDirection = CalcDirection();
        }

        private Vector3 CalcDirection()
        {
            Vector3 direction = _targetAttackPosition - this.transform.position;

            var distance = direction.magnitude;
            var directionNormalize = direction / distance;

            return directionNormalize;
        }


        private void OnCollisionEnter2D(Collision2D collision)
        {
            var planet = collision.collider.gameObject.GetComponent<PlanetComponent>();
            if (planet != null )
            {
                if (planet.CurrentPlanetBind == PlanetBind.Enemy ||
                    planet.CurrentPlanetBind == PlanetBind.Neutral )
                {
                    if( planet == _targetAttackPlanet)
                    {                        
                        planet.DamagePlanet(_damage);
                        this.ReturnToPool();
                    }
                }
                else if (planet.CurrentPlanetBind == PlanetBind.Player &&
                         planet == _targetAttackPlanet)
                {
                    planet.PumpPlanet(_damage);
                    this.ReturnToPool();
                }
            }
            
        }

        

    }
}
