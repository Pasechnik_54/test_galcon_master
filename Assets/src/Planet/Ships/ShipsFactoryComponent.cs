﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.src.Pool;
using Assets.src.Foundation;

using NetObject = System.Object;

namespace Assets.src.Planet.Ships
{
    [AddComponentMenu("Planet/ShipsFactoryComponent")]

    public class ShipsFactoryComponent : MonoBehaviour
    {
        // сколько кораблей за один цикл производится
        [SerializeField] private int AddShipsToOneSpawn = 5;
        [SerializeField] private float SpawnTimeSec = 1.0f;
        [SerializeField] private int MaxQuantityShips = 999;
        
        // общее количество кораблей в текущий момент
        public int QuantityShips { get { return _quantityShips; } }
        public void SetShipsCount(int shipsCount)
        {
            _quantityShips = shipsCount;
            ChangeQuantityShips?.Invoke(this, new EventArgs());
        }
        private int _quantityShips = 0;

        public EventHandler ChangeQuantityShips;
                       
        private TimersPool _timers;
        private CycleTimer _createTimer;

        


        private void Awake()
        {
            _timers = TimersPool.Instance;
            if (_timers == null)
            {
                throw new Exception("timers no instance");
            }
                        
        }

        
        public void RunCreateShips()
        {
            _createTimer = _timers.GetCycleTimer();
            _createTimer.Timeout += new EventHandler(OnSpawnShips);
            _createTimer.StartTimer(SpawnTimeSec);
        }

        public void RemoveShipsFromAttack(int shipsForAttack)
        {
            int remainsShips = QuantityShips - shipsForAttack;
            SetShipsCount(remainsShips);            

            Debug.Log(string.Format("removed {0} attack ships", shipsForAttack ) );
            
        }
        

               
        private void OnSpawnShips(NetObject sender, EventArgs args)
        {
            _quantityShips += AddShipsToOneSpawn;

            if (_quantityShips < MaxQuantityShips)
            {
                ChangeQuantityShips?.Invoke(this, new EventArgs());
            }            
            else
            {
                _quantityShips = MaxQuantityShips;
                ChangeQuantityShips?.Invoke( this, new EventArgs() );

                Debug.LogError("Overflow ships counter");
            }

            
        }


        private void OnDestroy()
        {
            if (_createTimer != null)
            {
                _createTimer.Timeout -= OnSpawnShips;
                _createTimer.ReturnToPool();
            }

        }




    }
}
