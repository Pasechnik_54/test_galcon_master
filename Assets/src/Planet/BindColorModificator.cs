﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.src.Planet
{
    [AddComponentMenu("Planet/PlanetColorModificator")]

    public class BindColorModificator : MonoBehaviour
    {
        // цвета выделения
        private Color _neutralColor = new Color(1, 1, 1, 1);
        private Color _playerColor = new Color(0, 0.5f, 1, 0.9f);
        private Color _enemyColor = new Color(1, 0, 1, 0.9f);

        private SpriteRenderer _currentRenderer = null;


        private void Awake()
        {
            _currentRenderer = GetComponent<SpriteRenderer>();
        }


        public void SetNeutralColor()  
        {
            Debug.Log(this.name + " set neutral color");
            _currentRenderer.color = _neutralColor;
        }

        public void SetPlayerColor() 
        {
            Debug.Log(this.name + " set player color");
            _currentRenderer.color = _playerColor;
        }

        public void SetEnemyColor()
        {
            Debug.Log(this.name + " set enemy color");
            _currentRenderer.color = _enemyColor;
        }



    }
}
