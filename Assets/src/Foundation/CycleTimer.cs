﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.src.Pool;

namespace Assets.src.Foundation
{
    public class CycleTimer : PoolObject
    {
        public EventHandler Timeout; // финиш таймера

        private const float DEFAULT_OVERFLOW_INTERVAL = 100.0f;

        private float _overflowTime = DEFAULT_OVERFLOW_INTERVAL;
        private float _countTime = 0;

        private bool _timerActive = false;

        public void StartTimer(float interval)
        {
            if (interval > 0)
            {
                _overflowTime = interval;
                _timerActive = true;
            }
            else
            {
                Debug.LogError("incorrect timer interval, this value clear to default");
                _overflowTime = DEFAULT_OVERFLOW_INTERVAL;
            }
        }

        private void Update()
        {
            if (_timerActive && _countTime < _overflowTime)
            {
                _countTime += Time.deltaTime;
            }
            else if (_timerActive && _countTime >= _overflowTime)
            {                
                Timeout?.Invoke(this, new EventArgs());
                _countTime = 0;
            }
        }

    }
}

