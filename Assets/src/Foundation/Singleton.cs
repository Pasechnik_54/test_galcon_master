using System;

using UnityEngine;

namespace Assets.src.Foundation
{
	public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
	{

		public static T Instance { get; private set; }


		void Awake()
		{			
			if ( Instance == null )
			{
				Instance = this as T;
				
			}
			else if ( (Instance != null) && (Instance != this) )
			{
				throw new Exception("Dublicate Singleton");
			}
		}

		void OnDestroy()
		{
			if (Instance == this)
			{
				Instance = null;
			}
		}
				
	}
}
